/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework1.ShapeThings;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Yaphetsf
 */
public class MySquare extends Rectangle implements SelectableNode {
//    Rectangle myThing;
    Paint p;

    
	public MySquare(double x, double y, double width, double height, Paint fill) {
        super(x, y, width, height);
        super.setFill(fill);
        super.setStroke(Color.BLACK);
        p = fill;
	}

    @Override
    public boolean requestSelection(boolean select) {
        return true;
    }

	@Override
	public void notifySelection(boolean select) {
		if(select){
			this.setStroke(Color.YELLOW);
                        this.toFront();
                }
		else
			this.setStroke(Color.BLACK);
	}
}
