/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework1.ShapeThings;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

/**
 *
 * @author Yaphetsf
 */
public class MyCircle extends Circle implements SelectableNode {
    Circle myThing;
    Paint p;
    public MyCircle(double centerX, double centerY, double radius, Paint fill) {
        super(centerX, centerY, radius, fill);
        super.setStroke(Color.BLACK);
        p = fill;
    }


	@Override
	public boolean requestSelection(boolean select) {
		return true;
	}

	@Override
	public void notifySelection(boolean select) {
		if(select){
			this.setStroke(Color.YELLOW);
                        this.toFront();
                }
		else
			this.setStroke(Color.BLACK);
	}
}
