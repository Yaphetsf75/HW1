/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework1;

import homework1.ShapeThings.MyCircle;
import homework1.ShapeThings.MySquare;
import homework1.ShapeThings.SelectionHandler;
import java.util.Random;
import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * z
 *
 * @author Yaphetsf
 */
public class HomeWork1 extends Application {

    private final BooleanProperty dragModeActiveProperty
            = new SimpleBooleanProperty(this, "dragModeActive", true);

    @Override
    public void start(Stage primaryStage) {

        Color deCo = Color.BLUE;
        Paint opCo = null;
        GridPane root = new GridPane();
        root.setHgap(8);
        root.setVgap(8);
        root.setPadding(new Insets(25));
        primaryStage.setMinWidth(850);
        primaryStage.setMaxHeight(700);
        primaryStage.setResizable(false);

        ColumnConstraints cons1 = new ColumnConstraints();
        cons1.setHgrow(Priority.NEVER);
        root.getColumnConstraints().add(cons1);

        ColumnConstraints cons2 = new ColumnConstraints();
        cons2.setHgrow(Priority.ALWAYS);

        root.getColumnConstraints().addAll(cons1, cons2);

        RowConstraints rcons1 = new RowConstraints();
        rcons1.setVgrow(Priority.NEVER);

        RowConstraints rcons2 = new RowConstraints();
        rcons2.setVgrow(Priority.ALWAYS);

        root.getRowConstraints().addAll(rcons1, rcons2);

        HBox hboxAnime = new HBox();
        HBox hboxSpec = new HBox();

        VBox vboxAnimeTool = new VBox();
        VBox vboxShapes = new VBox();

        BorderPane mainAnimationPane = new BorderPane();
        int X = 400;
        int Y = 500;
        mainAnimationPane.setMinHeight(Y);
        mainAnimationPane.setMinWidth(X);

        mainAnimationPane.setStyle("-fx-background-color: white;"
                + "-fx-border-color: black;"
                + "-fx-border-width: 1;"
                + "-fx-border-radius: 6;"
                + "-fx-padding: 6;");
        mainAnimationPane.setPadding(new Insets(100));

        Image imgExec = new Image(getClass().getResourceAsStream("Exe.png"));
        Button execute = new Button("Click to\n   run", new ImageView(imgExec));
        execute.setMinWidth(130);

        vboxAnimeTool.setStyle("-fx-background: #ffaadd;");

//        root.setGridLinesVisible(true);
        root.add(vboxAnimeTool, 0, 0, 1, 5);
        root.add(vboxShapes, 6, 0, 1, 6);
        root.add(hboxAnime, 0, 5, 3, 2);
        root.add(hboxSpec, 2, 5, 4, 2);
        root.add(mainAnimationPane, 1, 0, 5, 4);
        root.add(execute, 6, 6, 1, 1);

        // Adding buttons to boxes
        //vboxShapes
        Color defCol = Color.RED;

        Text vboxShapesText = new Text("Shapes");
        vboxShapesText.setCache(true);
        vboxShapesText.setFill(Color.CORNFLOWERBLUE);
        vboxShapesText.setFont(Font.loadFont(HomeWork1.class.getResource("BRADHITC.TTF").toExternalForm(), 25));
        DropShadow ds = new DropShadow();
        ds.setOffsetX(3.0);
        ds.setOffsetY(3.0);
        ds.setColor(Color.BROWN);
        vboxShapesText.setEffect(ds);
        Rectangle rect = new Rectangle(60, 60);
        rect.setFill(Color.TRANSPARENT);
        rect.setStroke(defCol);
        rect.setStrokeWidth(5);

        Image imgBtnShape1 = new Image(getClass().getResourceAsStream("Circle.png"));
        Image imgBtnShape2 = new Image(getClass().getResourceAsStream("Rectangle.png"));
        Image imgBtnShape3 = new Image(getClass().getResourceAsStream("Line.png"));
        Image imgBtnShape4 = new Image(getClass().getResourceAsStream("Rhombus.png"));
        Image imgBtnShape5 = new Image(getClass().getResourceAsStream("Picture.png"));

        Button butnShape1 = new Button("Circle", new ImageView(imgBtnShape1));
        Button butnShape2 = new Button("Rectangle", new ImageView(imgBtnShape2));
        Button butnShape3 = new Button("Line", new ImageView(imgBtnShape3));
        Button butnShape4 = new Button("Rhombus", new ImageView(imgBtnShape4));
        Button butnShape5 = new Button("Add an Image", new ImageView(imgBtnShape5));

        SelectionHandler selectionHandler = new SelectionHandler(mainAnimationPane);
        mainAnimationPane.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_PRESSED, selectionHandler.getMousePressedEventHandler());

        {
            butnShape1.setMinWidth(130);
            butnShape2.setMinWidth(130);
            butnShape3.setMinWidth(130);
            butnShape4.setMinWidth(130);
            butnShape5.setMinWidth(130);
        }

        ColorPicker colorPicker = new ColorPicker(Color.BROWN);
        colorPicker.setOnAction(new EventHandler() {

            @Override
            public void handle(Event event) {
                Paint fill = colorPicker.getValue();
                BackgroundFill backgroundFill
                        = new BackgroundFill(fill,
                                CornerRadii.EMPTY,
                                Insets.EMPTY);
                //Background background = new Background(backgroundFill);
                //root.setBackground(background);
            }
        });
        colorPicker.setPrefSize(130, 30);
        vboxShapes.getChildren().addAll(vboxShapesText, butnShape1, butnShape2, butnShape3, butnShape4,
                butnShape5, colorPicker);
        vboxShapes.setSpacing(30);
        vboxShapes.setPadding(new Insets(20, 20, 20, 20));

        //vboxAnimeTool
        Text vboxAnimeToolText = new Text("Animation\nTools");
        {
            vboxAnimeToolText.setFont(Font.loadFont(HomeWork1.class.getResource("BRADHITC.TTF").toExternalForm(), 25));
            vboxAnimeToolText.setCache(true);
            vboxAnimeToolText.setFill(Color.CORNFLOWERBLUE);
            vboxAnimeToolText.setStrokeWidth(30);
            Reflection r = new Reflection();
            r.setFraction(2);
            vboxAnimeToolText.setEffect(r);
        }

        Image imgBtnAnime1 = new Image(getClass().getResourceAsStream("RotateClock.png"));
        Image imgBtnAnime2 = new Image(getClass().getResourceAsStream("RotateCtrClk.png"));
        Image imgBtnAnime3 = new Image(getClass().getResourceAsStream("MoveVert.png"));
        Image imgBtnAnime4 = new Image(getClass().getResourceAsStream("MoveHori.png"));
        Image imgBtnAnime5 = new Image(getClass().getResourceAsStream("Zoom.png"));
        Image imgBtnAnime6 = new Image(getClass().getResourceAsStream("ChangeImage.png"));

        Button butnAnimeTool1 = new Button("Rotate\nclockwise", new ImageView(imgBtnAnime1));
        Button butnAnimeTool2 = new Button("Rotate\ncounter\nclockwise", new ImageView(imgBtnAnime2));
        Button butnAnimeTool3 = new Button("Moving\nup-down", new ImageView(imgBtnAnime3));
        Button butnAnimeTool4 = new Button("Moving\nright left", new ImageView(imgBtnAnime4));
        Button butnAnimeTool5 = new Button("Zoom\nin-out", new ImageView(imgBtnAnime5));
        Button butnAnimeTool6 = new Button("Change\nimage", new ImageView(imgBtnAnime6));

        {
            butnAnimeTool1.setMinHeight(50);
            butnAnimeTool2.setMinHeight(60);
            butnAnimeTool3.setMinHeight(50);
            butnAnimeTool4.setMinHeight(50);
            butnAnimeTool5.setMinHeight(50);
            butnAnimeTool6.setMinHeight(50);

            butnAnimeTool1.setMinWidth(100);
            butnAnimeTool2.setMinWidth(100);
            butnAnimeTool3.setMinWidth(100);
            butnAnimeTool4.setMinWidth(100);
            butnAnimeTool5.setMinWidth(100);
            butnAnimeTool6.setMinWidth(100);
        }

        //A checkbox without a caption
        CheckBox repeatable = new CheckBox("Anime repeats");
        repeatable.setSelected(true);

        // force the field to be numeric only
        TextField timeField = new TextField();
        timeField.setPromptText("Time to play");

        timeField.setCursor(Cursor.TEXT);
//        timeField.setOnKeyPressed(new EventHandler<e>);

        vboxAnimeTool.getChildren().addAll(vboxAnimeToolText, butnAnimeTool1,
                butnAnimeTool2, butnAnimeTool3,
                butnAnimeTool4, butnAnimeTool5, butnAnimeTool6,
                repeatable, timeField);
        vboxAnimeTool.setSpacing(15);
        vboxAnimeTool.setPadding(new Insets(5, 5, 5, 5));

        Scene scene = new Scene(root);

//        scene.getStylesheets().
//                add(HomeWork1.class.getResource("CSS_File.css").toExternalForm()); 
        primaryStage.setTitle("MyProgram");
        primaryStage.setScene(scene);
        primaryStage.show();

        butnShape1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Random rand = new Random();
                int x = rand.nextInt(X) + 1;
                int y = rand.nextInt(Y) + 1;
                Node circleTest = makeDraggable(new MyCircle(x, y, 40, colorPicker.getValue()));
                mainAnimationPane.getChildren().addAll(circleTest);
//                x = rand.nextInt(400) + 1;
//                y = rand.nextInt(500) + 1;
            }
        });

        butnShape2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                Random rand = new Random();
                int x = rand.nextInt(X) + 1;
                int y = rand.nextInt(Y) + 1;
                Node suqareTest = makeDraggable(new MySquare(x, y, 50, 50, colorPicker.getValue()));
                mainAnimationPane.getChildren().addAll(suqareTest);
            }
        });

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private Node makeDraggable(final Node node) {
        final DragContext dragContext = new DragContext();
        final Group wrapGroup = new Group(node);

//        wrapGroup.addEventFilter(
//                MouseEvent.ANY,
//                new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(final MouseEvent mouseEvent) {
//                if (true) {
//                    // disable mouse events for all children
//                    mouseEvent.consume();
//                }
//            }
//        });se

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent mouseEvent) {
                if (true) {
                    // remember initial mouse cursor coordinates
                    // and node position
                    dragContext.mouseAnchorX = mouseEvent.getX();
                    dragContext.mouseAnchorY = mouseEvent.getY();
                    dragContext.initialTranslateX
                            = node.getTranslateX();
                    dragContext.initialTranslateY
                            = node.getTranslateY();
                    wrapGroup.toFront();
                }
            }
        });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(final MouseEvent mouseEvent) {
                if (true) {
                    // shift node from its initial position by delta
                    // calculated from mouse cursor movement
                    node.setTranslateX(
                            dragContext.initialTranslateX
                            + mouseEvent.getX()
                            - dragContext.mouseAnchorX);
                    node.setTranslateY(
                            dragContext.initialTranslateY
                            + mouseEvent.getY()
                            - dragContext.mouseAnchorY);
                }
            }
        });

        return wrapGroup;
    }

    private static final class DragContext {

        public double mouseAnchorX;
        public double mouseAnchorY;
        public double initialTranslateX;
        public double initialTranslateY;
    }
}
